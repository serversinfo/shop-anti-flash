#include <sdktools>
#include <shop>

#define CATEGORY	"stuff"
#define VERSION		"1.0.8"

public Plugin myinfo = 
{
	name		= "[Shop] Anti-flash",
	author		= "ShaRen",
	version		= VERSION,
	url			= "Servers-Info.Ru"
}

bool g_bAntiFlash[MAXPLAYERS+1];
ConVar g_hPrice;
ConVar g_hSellPrice;
ItemId g_id;

//int g_FlashOwner = -1;

#define ALPHA_SET 0.5
int g_iFlashAlpha = -1;


public OnPluginStart()
{
	//HookEvent("flashbang_detonate", Event_Flashbang_detonate);
	HookEvent("player_blind", Event_Flashed);
	
	g_iFlashAlpha = FindSendPropInfo("CCSPlayer", "m_flFlashMaxAlpha");
	if (g_iFlashAlpha == -1 )
		SetFailState("Failed to find \"m_flFlashMaxAlpha\".");
	// SHOP
	g_hPrice = CreateConVar("sm_shop_antiflash_price", "28000", "Стоимость покупки Anti-flash.");
	HookConVarChange(g_hPrice, OnConVarChange);
	
	g_hSellPrice = CreateConVar("sm_shop_antiflash_sellprice", "26000", "Стоимость продажи Anti-flash.");
	HookConVarChange(g_hPrice, OnConVarChange);

	AutoExecConfig(true, "shop_antiflash", "shop");
	
	if (Shop_IsStarted()) Shop_Started();
}

public OnConVarChange(ConVar hCvar, const char[] oldValue, const char[] newValue)
{
	if(g_id != INVALID_ITEM) {
		if(hCvar == g_hPrice) Shop_SetItemPrice(g_id, GetConVarInt(hCvar));
		else if(hCvar == g_hSellPrice) Shop_SetItemSellPrice(g_id, GetConVarInt(hCvar));
	}
}

public OnPluginEnd() Shop_UnregisterMe();

public void Shop_Started()
{
	CategoryId category_id = Shop_RegisterCategory(CATEGORY, "Дополнительно", "");
	
	if (Shop_StartItem(category_id, "antiflash")) {
		Shop_SetInfo("[CT-Only] Anti-flash", "Вас не смогут ослепить световой гранатой (только для КТ)", GetConVarInt(g_hPrice), GetConVarInt(g_hSellPrice), Item_Togglable, -1);
		Shop_SetCallbacks(OnItemRegistered, OnItemUsed);
		Shop_EndItem();
	}
}

public void OnItemRegistered(CategoryId category_id, const char[] category, const char[] item, ItemId item_id)
{
	g_id = item_id;
}

public ShopAction OnItemUsed(int iClient, CategoryId category_id, const char[] category, ItemId item_id, const char[] item, bool isOn, bool elapsed)
{
	if (isOn || elapsed) {
		g_bAntiFlash[iClient] = false;
		return Shop_UseOff;
	}

	g_bAntiFlash[iClient] = true;

	return Shop_UseOn;
}

public void OnClientPostAdminCheck(int iClient) 
{
	g_bAntiFlash[iClient] = false;
}

//public Action Event_Flashbang_detonate(Handle:event, const String:name[], bool:dontBroadcast)
//{
//	int client = GetClientOfUserId(GetEventInt(event,"userid"));
//	
//	if( !client || !IsClientConnected(client) || !IsClientInGame(client) || !IsPlayerAlive(client) )
//	{
//		g_FlashOwner = -1; 
//		return Plugin_Continue;
//	}
//	
//	g_FlashOwner = client;
//	
//	return Plugin_Continue;
//}


public Action Event_Flashed(Event e, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(e.GetInt("userid"));

	if (!client || !IsClientInGame(client))
		return Plugin_Continue;

	if (!IsPlayerAlive(client) || (g_bAntiFlash[client] && GetClientTeam(client) == 3)) 
	{ 
		//PrintToChat(client, "Flash!");
		SetEntDataFloat(client, g_iFlashAlpha, ALPHA_SET);
		return Plugin_Continue;
	}
	
	//CreateTimer(0.01, BlindTime, client);
	return Plugin_Continue;
}

//public Action BlindTime(Handle timer, any client)
//{
//	if ( g_FlashOwner != -1 && client != g_FlashOwner && IsClientInGame(client)  && IsPlayerAlive(client) && IsClientInGame(g_FlashOwner) && IsPlayerAlive(g_FlashOwner) && GetClientTeam(client) == GetClientTeam(g_FlashOwner) )
//		SetEntDataFloat(client, g_iFlashAlpha, ALPHA_SET);
//	
//	return Plugin_Stop;
//}